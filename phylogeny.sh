datasets download genome accession $(cat phylogeny_accessions.txt) --include proteins

#From Orthofinder tools - extract longest protein per gene
for f in ./ncbi_datasets/data/*faa;do
	python ./Orthofinder/tools/primary_transcript.py $f
done

#Run Orthofinder to perform alignments and extract single-copy orthologs
singularity exec -B /scratch/brown/ /scratch/brown/progs/orthofinder_2.5.5.2.sif orthofinder -t 24 -a 24 -f ./ncbi_datasets/data/primary_transcripts/

#Perform multiple-sequence alignment with MAFFT
in_dir=ncbi_dataset/data/primary_transcripts/OrthoFinder/Results/Single_Copy_Orthologue_Sequences/
for f in ${in_dir}/*;do
	SID=$(basename ${f%.fa})
	mafft --auto --amino --maxiterate 1000 --thread 1 ${f} > mafft_out/${SID}_mafft.fasta
don

#Trim alignments with trimAL
INPUTS=$(pwd)/mafft_out
OUTDIR=$(pwd)/trimAL_out
for i in ${INPUTS}/*mafft.fasta; do
	echo $i
	trimal -in ${i} -out ${OUTDIR}/$(basename ${i%.fasta})_mafft_trimmed.fasta -automated1; 
done

#Create Supermatrix with GeneStitcher
INPUTS=$(pwd)/trimAL_out
OUTDIR=$(pwd)/geneStitcher_out

python2 geneStitcher.py -in $INPUTS/*trimmed.fasta -d '_'

#Format partitions for iq-tree with fixPartitions script from genestitcher
python ./geneSticher/fixPartition.py Partition.txt Partition_fixed.txt

#Create phylogenetic tree with IQ-TREE
iqtree2/build/iqtree2 -s SuperMatrix.fas -p Partition_fixed.txt --prefix iqtree_v1 -B 1000 -T 24

#Run mcmctree with bounds from timetree to calibrate phylogenetic tree
mcmctree mcmctree.ctl
