# Pipistrellus nathusii Genome Assembly and Protein-coding Annotation

Collection of scripts used to generate the assemblies for both haplotypes of the nuclear genome as well as the mitochondrial genome and a protein-coding annotation of the primrary haplotype

## Preprocessing reads

Commands used to pre-process sequencing reads can be found here: `read_preprocessing.sh`

## Mitochondrial assembly

Command used to generate the mitochrondrial genome can be found here: `mito_assembly.sh`

## Nuclear Genome Assembly

Commands used to generate the two haplotype assemblies can be found here: `genome_assembly.sh`

## Protein-Coding Annotation

Commands used to generate protein-coding annotation can be found here: `annotation.sh`

## Generating plots

There are various R plots included here for plotting statistics related to the contig and scaffold lengths, BUSCO scores and number of intact TOGA projections.

## Phylogenetic tree

These contain the commands used to generate the phylogentic tree as well as input files for mcmctree. Commands can be found here: `phylogeny.sh` with accessory files: `mcmctree.ctl` and `iqtree_v1.contree`
