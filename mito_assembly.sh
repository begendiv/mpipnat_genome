# Mitochondrial Genome assembly

## INSTALLED TOOLS
oatk: https://github.com/c-zhou/oatk
OatkDB: https://github.com/c-zhou/OatkDB
hmmer: http://eddylab.org/software/hmmer/hmmer.tar.gz

/progs/oatk/oatk -k 1001 -c 30 -t 8 --nhmmscan /progs/oatk/hmmer-3.4/local/bin/nhmmscan -m /progs/OatkDB/v20230921/mammalia_mito.fam -o mPipNat1 /asm_mPipNat/data/pacbio/hifi/m64046_210410_183312_DV.trimmed.fq.gz /asm_mPipNat/data/pacbio/hifi/m64046_210404_070108_DV.trimmed.fq.gz /asm_mPipNat/data/pacbio/hifi/m64046_210315_101819_DV.trimmed.fq.gz /asm_mPipNat/data/pacbio/hifi/m64046_210410_183312_DV.trimmed.fq.gz


