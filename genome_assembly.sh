# Nuclear Genome Assembly

## Generate intial contigs with hifiasm
MAXCOV=$(cat genomescope_out/Maximum_depth)
./hifiasm -o mPipNat.hifiasm -t 48 -l3 --primary --purge-max $MAXCOV --h1 hic_R1.fastq.gz --h2 hic_R2.fastq.gz hifi_data/*_DV_TRIMMED.fastq.gz

awk '/^S/{print ">"$2;print $3}' mPipNat.hifiasm.hic.hap1.p_ctg.gfa > mPipNat.hifiasm.hic.hap1.p_ctg.fa
awk '/^S/{print ">"$2;print $3}' mPipNat.hifiasm.hic.hap2.p_ctg.gfa > mPipNat.hifiasm.hic.hap2.p_ctg.fa

## Remove any retained haplotigs using purge-dups - run for each haplotype

HAP1=mPipNat.hifiasm.hic.hap1.p_ctg.fa

mkdir -p $OUT_DIR/hap1

#A: align HiFi reads to hap1 and generate BAM files, then calculate read depth histogram and base-level read depth
cd $OUT_DIR/hap1
for i in $(ls hifi_data/*.trimmed.fastq.gz)
do
        READ_NAME=$(basename $i .fastq.gz)
        minimap2 -xmap-hifi -I 4G -t 24 $HAP1 $i | gzip -c - > $OUT_DIR/hap1/$READ_NAME\.paf.gz
done
pbcstat *.paf.gz #(produces PB.base.cov and PB.stat files)
calcuts PB.stat > cutoffs_default 2>calcults_default.log
python3 $HIST_PLOT -c cutoffs_default PB.stat PB_default.png

calcuts -m$TRANS -u$MAX_DEPTH PB.stat > cutoffs_$CUTOFFS
python3 $HIST_PLOT -c cutoffs_$CUTOFFS PB.stat PB_$CUTOFFS\.png

#B: Split an assembly and do a self-self alignment
cd $OUT_DIR/hap1
split_fa $HAP1 > $HAP1_NAME\.split
minimap2 -t 18 -xasm5 -DP $HAP1_NAME\.split $HAP1_NAME\.split | pigz -p 6 -c - > $HAP1_NAME\.split.self.paf.gz

#C: Purge haplotigs and overlaps
cd $OUT_DIR/hap1
purge_dups -2 -T cutoffs_default -c PB.base.cov $HAP1_NAME\.split.self.paf.gz > dups_default.bed 2> purge_dups_default.log
purge_dups -2 -T cutoffs_$CUTOFFS -c PB.base.cov $HAP1_NAME\.split.self.paf.gz > dups_$CUTOFFS\.bed 2> purge_dups_$CUTOFFS\.log

#D: Get purged primary and haplotig sequences from draft assembly
cd $OUT_DIR/hap1
get_seqs -e dups_default.bed $HAP1
get_seqs -e dups_$CUTOFFS\.bed $HAP1

# Map HiC reads to contig assembly - run for each haplotype.
# Perl scripts taken from VGP arima mapping pipeline https://github.com/VGP/vgp-assembly/tree/master/pipeline/salsa

PREFIX=mPipNat.hap1
FILTER="filter_five_end.pl"
COMBINER="two_read_bam_combiner.pl"
DEDUP="dedup.sh"

bwa mem -t 32 -B8 ${PREFIX}.purged.fa hic_R1.fastq.gz | samtools view -Sb > ${PREFIX}_1.bam
samtools view -h ${PREFIX}_1.bam | perl $FILTER | samtools view -@ 32 -Sb > ${PREFIX}_filt_1.bam

bwa mem -t 32 -B8 ${PREFIX}.purged.fa hic_R2.fastq.gz | samtools view -Sb > ${PREFIX}_2.bam
samtools view -h ${PREFIX}_2.bam | perl $FILTER | samtools view -@ 32 -Sb > ${PREFIX}_filt_2.bam

perl $COMBINER ${PREFIX}_filt_1.bam $FILT_DIR/${PREFIX}_filt_2.bam | samtools view -@ 32 -Sb > ${PREFIX}_merge.bam

bash $DEDUP ${PREFIX}_merge.bam 32

# Perform scaffolding with YaHS

yahs ${PREFIX}.purged.fa ${PREFIX}_merge.bed -o ${PREFIX}

# Create higlass image of two combined haplotypes for manual curation

bwa-mem2 index mPipNat_merged.fasta
bwa-mem2 mem -5SP -T0 -t48 mPipNat2_merged.fasta hic_R1.fastq.gz hic_R2.fastq.gz -o mPipNat_combined.bwa.sam
pairtools parse --output-stats mPipNat_combined.parse.stats --min-mapq 0 --walks-policy 5unique --max-inter-align-gap 30 --nproc-in 48 --nproc-out 48 --chroms-path mPipNat_merged.genome mPipNat_combined.bwa.sam > mPipNat_combined.bwa.parsed.pairsam
pairtools sort --nproc 48 --tmpdir=./combined/ mPipNat_combined.bwa.parsed.pairsam > mPipNat_combined.bwa.sorted.pairsam
pairtools dedup --nproc-in 48 --nproc-out 48 --mark-dups --output-stats mPipNat_combined.dedup.stats --output mPipNat_combined.bwa.dedup.pairsam mPipNat_combined.bwa.sorted.pairsam
pairtools split --nproc-in 48 --nproc-out 48 --output-pairs mPipNat_combined.bwa.dedup.pairs --output-sam mPipNat_combined.bwa.dedup.bam mPipNat_combined.bwa.dedup.pairsam
bgzip mPipNat_combined.bwa.dedup.pairs
pairix mPipNat_combined.bwa.dedup.pairs.gz
cooler cload pairix -p 48 mPipNat_merged.genome:1000 mPipNat_combined.bwa.dedup.pairs.gz mPipNat_combined.1000.cool
cooler zoomify -p 48 mPipNat_combined.1000.cool

