# Protein-Coding Genome Annotation

# Map RNAseq data to genome

hisat2-build mPipNat mPipNat.fasta
hisat2 --dta -x mPipNat --no-mixed --no-discordant -p 12 -1 RNA_R1.fastq.gz -2 RNA_R2.fastq.gz > RNA_hisat.bam
samtools view -F 3852 RNA_hisat.sam | samtools sort > RNA_hisat.bam

# Create de-novo transcripts with stringtie

stringtie -o mPipNat_stringtie.gtf RNA_hisat.bam

# Create CDS annotation using TransDecoder

util/gtf_genome_to_cdna_fasta.pl mPipNat_stringtie.gtf mPipNat.fasta > transcripts.fasta
util/gtf_to_alignment_gff3.pl transcripts.gtf > transcripts.gff3
TransDecoder.LongOrfs -t transcripts.fasta
TransDecoder.Predict -t transcripts.fasta 
util/cdna_alignment_orf_to_genome_orf.pl transcripts.fasta.transdecoder.gff3 transcripts.gff3 transcripts.fasta > mPipNat_stringtie_transDecoder.gff3

# Perform repeat masking
singularity exec tetools_latest.sif
BuildDatabase -name mPipNat mPipNat.fa
RepeatModeler -database mPipNat -threads 24 -LTRStruct
famdb.py -i RepeatMaskerLib.h5 families --format fasta_name --include-class-in-name --ancestors --descendants "pipistrellus nathusii" > mPipNat-rm.fa
cat mPipNat-rm.fa mPipNat-families.fa > mPipNat_combined.fa
RepeatMasker -pa 4 -a -s -gccalc -xsmall -gff -lib mPipNat_combined.fa mPipNat 

# Run Braker3 to obtain gene predictions
singularity exec BRAKER-3.0.2/braker3.sif braker.pl --genome=mPipNat.fasta.masked --AUGUSTUS_ab_initio --gff3 --bam=RNA_hisat.bam --prot_seq=orthodb_vertebrata.fa
singularity run evidencemodeler_latest.sif & EVidenceModeler/EvmUtils/misc/braker_GTF_to_EVM_GFF3.pl braker/braker.gtf > braker.evm.gff3

# Run TOGA to obtain ortholog projections from hg38 human genome
# make_lastz_chains obtained from here: https://github.com/hillerlab/make_lastz_chains
# TOGA obtained from here: https://github.com/hillerlab/TOGA

### Minimal example
./make_chains.py hg38 mPipNat hg38.fa mPipNat.fa --executor slurm --project_dir test
toga.py test_input/hg38.mm10.chr11.chain hg38.bed hg38.2bit mPipNat.2bit --kt --pn test -i supply/hg38.wgEncodeGencodeCompV34.isoforms.txt --nc ${path_to_nextflow_config_dir} --cb 3,5 --cjn 500 --u12 supply/hg38.U12sites.tsv --ms
./postoga/bed2gff/target/release/bed2gff query_annotation.bed query_isoform.tsv query_annotation.gff
singularity run evidencemodeler_latest.sif & EVidenceModeler/EvmUtils/misc/braker_GTF_to_EVM_GFF3.pl query_annotation.gff > toga.evm.gff3

# Combine evidences into a single annotation

cat mPipNat_stringtie_transDecoder.gff3 braker.evm.gff3 toga.evm.gff3 > evidences.gff3

singularity run evidencemodeler_latest.sif & EVidenceModeler/EvmUtils/partition_EVM_inputs.pl --genome mPipNat.fasta --gene_predictions evidences.gff3 --segmentSize 100000000 --overlapSize 1000000 --partition_listing partitions_list.out --partition_dir partitions
singularity run evidencemodeler_latest.sif & EVidenceModeler/EvmUtils/write_EVM_commands.pl --genome mPipNat.fasta --weights weights.txt --gene_predictions evidences.gff3 --output_file_name evm.out --partitions partitions_list.out > commands.list
singularity run evidencemodeler_latest.sif & sh commands.list
singularity run evidencemodeler_latest.sif & EVidenceModeler/EvmUtils/recombine_EVM_partial_outputs.pl --partitions partition_list.out --output_file_name evm.out
singularity run evidencemodeler_latest.sif & EVidenceModeler/EvmUtils/convert_EVM_outputs_to_GFF3.pl --partitions partition_list.out --output_file_name evm.out --genome mPipNat.fasta

# Obtain functional annotation
agat_sp_extract_sequences.pl evm.out.gff3 -f mPipNat.fa -p -o evm.prot.fa
interproscan.sh -i evm.prot -t p -dp -pa -appl Pfam,ProDom-2006.1,SuperFamily-1.75 --goterms --iprlookup
diamond blastp -d /scratch/brown/db/swissprot/uniprot_sprot.dmnd --threads 48 --outfmt 6 --out evm.prot.blastp.swissprot.out --query evm.prot.fa
agat_sp_manage_functional_annotation.pl -f interproscan_manage_FA/evm.out.gff3 -b evm.prot.blastp.uniref50.out --db /scratch/brown/db/uniref/uniref50.fasta -o merge_manage_FA
agat_sp_manage_functional_annotation.pl -f interproscan_manage_FA/evm.out.gff3 -b evm.prot.blastp.swissprot.out --db /scratch/brown/db/swissprot/uniprot_sprot.fasta -o swissprot_manage_FA --ID PIPNAT
