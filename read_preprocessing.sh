# Read Pre-Processing

# Create HiFi reads from raw PacBio subreads

ccs --min-rq=0.88 subreads.bam ccs.bam
actc subreads.bam ccs.bam subreads_to_ccs.bam
deepconsensus run --subreads_to_ccs=subreads_to_ccs.bam --ccs_bam=ccs.bam --checkpoint=./model/checkpoint --output deepconsensus.out.fastq
gzip deepconsensus.out.fastq

# Filter adapter sequences from HiFi reads

cutadapt -o deepconsensus.trimmed.out.fastq deepconsensus.out.fastq.gz -b ATCTCTCTCAACAACAACAACGGAGGAGGAGGAAAAGAGAGAGAT -b ATCTCTCTCTTTTCCTCCTCCTCCGTTGTTGTTGTTGAGAGAGAT --discard-trimmed
gzip deepconsensus.trimmed.out.fastq

# Build kmer database and construct genomescope model
meryl k=31 memory=64 threads=32 count deepconsensus.trimmed.out.fastq output mPipNat.meryl
meryl histogram mPipNat.meryl | sed 's/\t/ /g' > mPipnat.hist
Rscript genomescope2.0/genomescope.R -i mPipNat.hist -o genomescope_out -k 31 --fitted_hist
# Obtain values from fitted distributions
cd genomescope_out
VAR="$(grep -n "Genome Haploid Length" summary.txt | cut -f1 -d:)"
sed -n $VAR\p summary.txt | sed -e 's/  \+/\t/g' | cut -f3 | sed -e 's/,//g' | sed -e 's/ bp//g' > Estimated_genome_size
VAR="$(grep -n "kmercov " model.txt | cut -f1 -d:)"
KCOV="$(printf "%.2f\n" $(sed -n $VAR\p model.txt | sed -e 's/ \+/\t/g' | cut -f2))"
printf "%.0f\n" $(echo "$KCOV * 1.5" | bc) > Transition_parameter
printf "%.0f\n" $(echo ""$(cat Transition_parameter)" * 3" | bc) > Maximum_depth

# Filter Illumina HiC reads for read quality and trim adapter sequences

trim_galore --paired --fastqc --quality 20 --cores 8 -o hic_trimmed/ hic_R1.fastq.gz hic_R2.fastq.gz


